# flake8: noqa: F401

"""Top-level package for timeplots."""

__author__ = """Greg Mueller"""
__email__ = "greg@grelleum.com"
__version__ = "0.1.4"

from .timeplots import Plotter, TimeParser, missing_time_data
