=========
timeplots
=========


.. image:: https://img.shields.io/pypi/v/timeplots.svg
        :target: https://pypi.python.org/pypi/timeplots

.. image:: https://img.shields.io/travis/grelleum/timeplots.svg
        :target: https://travis-ci.com/grelleum/timeplots

.. image:: https://readthedocs.org/projects/timeplots/badge/?version=latest
        :target: https://timeplots.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/grelleum/timeplots/shield.svg
     :target: https://pyup.io/repos/github/grelleum/timeplots/
     :alt: Updates



Bokeh wrapper for creating time based line plots.


* Free software: MIT license
* Documentation: https://timeplots.readthedocs.io.


Features
--------

timeplots contains some tools for creating multiline plots where the x-axis are datetime values.


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
